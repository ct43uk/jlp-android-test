package com.johnlewis.ctdemo

import com.johnlewis.ctdemo.data.DtoValueConverters
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test

class DtoValueConvertersTest {

    @Test
    fun testBuildImageUrlWithoutSlash() {
        val url = "example.com/image.png"
        val expected = "https://example.com/image.png"
        val actual = DtoValueConverters.buildImageUrl(url)
        assertEquals(expected, actual)
    }

    @Test
    fun testBuildImageUrlWithSlash() {
        val url = "//example.com/image.png"
        val expected = "https://example.com/image.png"
        val actual = DtoValueConverters.buildImageUrl(url)
        assertEquals(expected, actual)
    }

    @Test
    fun testMapCurrencyCodeToSymbol() {
        val usdCode = "USD"
        val usdSymbol = "$"
        val actualUsdSymbol = DtoValueConverters.mapCurrencyCodeToSymbol(usdCode)
        assertEquals(usdSymbol, actualUsdSymbol)

        val eurCode = "EUR"
        val eurSymbol = "€"
        val actualEurSymbol = DtoValueConverters.mapCurrencyCodeToSymbol(eurCode)
        assertEquals(eurSymbol, actualEurSymbol)

        val gbpCode = "GBP"
        val gbpSymbol = "£"
        val actualGbpSymbol = DtoValueConverters.mapCurrencyCodeToSymbol(gbpCode)
        assertEquals(gbpSymbol, actualGbpSymbol)

        val invalidCode = "INVALID"
        val actualInvalidSymbol = DtoValueConverters.mapCurrencyCodeToSymbol(invalidCode)
        assertNull(actualInvalidSymbol)
    }
}