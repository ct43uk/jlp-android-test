package com.johnlewis.ctdemo.data.remote

data class ProductDetailResponse(
    val detailsData: List<Detail>
)

data class Detail(
    val productId: String,
    val title: String,
    val defaultSku: String,
    val details: Details,
    val features: List<Feature>,
    val price: Price,
    val media: Media
)

data class Media(
   val images: Image
)

data class Image(
    val altText: String,
    val urls: List<String>
)

data class Feature(
    val groupName: String,
    val attributes: List<Attribute>
)

data class Attribute(
    val value: String,
    val values: List<String>,
    val multivalued: Boolean,
    val id: String,
    val name: String,
    val toolTip: String
)

data class Details(
    val returns: String,
    val returnsHeadline: String,
    val termsAndConditions: String,
    val productInformation: String,
    val features: List<Feature>,
    val editorsNotes: String,
    val weLikeItBecause: String
)
