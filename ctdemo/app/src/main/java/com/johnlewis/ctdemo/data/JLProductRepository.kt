package com.johnlewis.ctdemo.data

import android.app.Application
import com.google.gson.Gson
import com.johnlewis.ctdemo.R
import com.johnlewis.ctdemo.data.remote.JLProductsSearchService
import com.johnlewis.ctdemo.data.remote.ProductDetailResponse
import com.johnlewis.ctdemo.data.remote.ProductSearchResponse


data class ProductResult<T>(
    val data: T? = null,
    val errorCode: Int? = null
)

class JLProductRepository constructor(
    private val application: Application
) {
    suspend fun searchByKeyword(query: String): ProductResult<List<ListItemDto>> {
//        return fromRemote(query)
        // API gave me 403 after a while, so I switched to loading the static data
        return searchFromStaticData()
    }

    private fun searchFromStaticData() : ProductResult<List<ListItemDto>> {
        val inputStream = application.resources.openRawResource(R.raw.data)
        val jsonString = inputStream.bufferedReader().use { it.readText() }
        val jsonObj = Gson().fromJson(jsonString, ProductSearchResponse::class.java)
        return ProductResult(data = jsonObj.products.map { ListItemDto.fromApiProduct(it) })
    }

    private suspend fun searchFromRemote(query: String) : ProductResult<List<ListItemDto>> {
        val response = JLProductsSearchService.build().searchByKeyword(query = query)
        return if (response.isSuccessful && response.body() != null) {
            ProductResult(data = response.body()!!.products.map { ListItemDto.fromApiProduct(it) })
        } else {
            ProductResult(errorCode = response.code())
        }
    }

    suspend fun detailFromProductId(id: String): ProductResult<ProductDetailDto> {
        val inputStream = application.resources.openRawResource(R.raw.data2)
        val jsonString = inputStream.bufferedReader().use { it.readText() }
        val jsonObj = Gson().fromJson(jsonString, ProductDetailResponse::class.java)
        val detail = jsonObj.detailsData.firstOrNull { it.productId == id }
        detail?.let {
            return ProductResult(data = ProductDetailDto.fromAPiProduct(detail))
        } ?: return ProductResult(errorCode = 404)
    }
}