package com.johnlewis.ctdemo.ui.productdetail

import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.johnlewis.ctdemo.data.ProductDetailDto
import com.johnlewis.ctdemo.databinding.FragmentProductDetailBinding


class ProductDetailFragment : Fragment() {

    private var _binding: FragmentProductDetailBinding? = null
    private val binding get() = _binding!!

    private lateinit var viewModel: ProductDetailViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentProductDetailBinding.inflate(inflater, container, false)
        viewModel = ViewModelProvider(this)[ProductDetailViewModel::class.java]
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addObserver()
        viewModel.loadDetail("3218074")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun addObserver() {
        viewModel.state.observe(viewLifecycleOwner) { state ->
            if (state.data != null) {
                applyDataToUI(state.data)
            }
            if (state.errorStringId != null) {
                Toast.makeText(requireContext(), state.errorStringId, Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun applyDataToUI(data: ProductDetailDto) {
        binding.title.text = data.title
        binding.productImagesViewPager.adapter = ProductImagesAdapter(requireContext(), data.imageUrls)
        binding.price.text = data.price
        binding.productInfo.text = Html.fromHtml(data.productInfoHtml)
    }
}