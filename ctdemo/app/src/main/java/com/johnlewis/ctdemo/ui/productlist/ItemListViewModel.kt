package com.johnlewis.ctdemo.ui.productlist

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.johnlewis.ctdemo.data.JLProductRepository
import com.johnlewis.ctdemo.data.ListItemDto
import com.johnlewis.ctdemo.data.ApiErrorCodeToMessageMapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

data class UiState(
    val isLoading: Boolean = false,
    val errorStringId: Int? = null,
    val data: List<ListItemDto>? = null
)

class ItemListViewModel(private val application: Application) : AndroidViewModel(application) {

    private val _state = MutableLiveData<UiState>()
    val state: LiveData<UiState> = _state

    init {
        _state.postValue(UiState(isLoading = true))

        android.os.Handler().postDelayed({
            fetchItems()
        }, 2000)
    }

    private fun fetchItems() {
        viewModelScope.launch(Dispatchers.IO) {
            val result = JLProductRepository(application).searchByKeyword(query = "Dishwasher")
            if (result.data != null) {
                _state.postValue(UiState(data = result.data!!))
            } else {
                _state.postValue(UiState(errorStringId = ApiErrorCodeToMessageMapper.parse(result.errorCode!!)))
            }
        }
    }
}