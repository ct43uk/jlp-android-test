package com.johnlewis.ctdemo.ui.productlist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.johnlewis.ctdemo.data.ListItemDto
import com.johnlewis.ctdemo.R
import com.squareup.picasso.Picasso

class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val imageView: ImageView = itemView.findViewById(R.id.image)
    val titleText: TextView = itemView.findViewById(R.id.title)
    val priceText: TextView = itemView.findViewById(R.id.price)
}

 class ItemListAdapter(private var items: List<ListItemDto>, val onItemSelected: (productId: String) -> (Unit)) : RecyclerView.Adapter<MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_layout, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = items[position]
        Picasso.get().load(item.imageSrc).into(holder.imageView)
        holder.titleText.text = item.title
        holder.priceText.text = item.price
        holder.itemView.setOnClickListener {
            onItemSelected(item.productId)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

     fun updateItems(itemList: List<ListItemDto>) {
         items = itemList
         notifyDataSetChanged() // note: could use a diff mechanism in scenarios that warranted it
     }
}
