package com.johnlewis.ctdemo.data.remote


data class ProductSearchResponse(
    val showInStockOnly: Boolean,
    val products: List<Product>
)

data class Product(
    val productId: String,
    val type: String,
    val title: String,
    val htmlTitle: String,
    val code: String,
    val averageRating: Float,
    val reviews: Int,
    val price: Price,
    val image: String,
    val alternativeImageUrls: List<String>,
    val displaySpecialOffer: String
)

data class Price(
    val was: String,
    val then1: String,
    val then2: String,
    val now: String,
    val uom: String,
    val currency: String
)
