package com.johnlewis.ctdemo.ui.productlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.johnlewis.ctdemo.databinding.FragmentItemListBinding


class ItemListFragment : Fragment() {

    private var _binding: FragmentItemListBinding? = null
    private val binding get() = _binding!!

    private lateinit var itemListAdapter: ItemListAdapter
    private lateinit var viewModel: ItemListViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentItemListBinding.inflate(inflater, container, false)
        viewModel = ViewModelProvider(this)[ItemListViewModel::class.java]
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecycler()
        addObserver()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setupRecycler() {
        itemListAdapter = ItemListAdapter(emptyList(), onItemSelected = {
            val action = ItemListFragmentDirections.actionItemListFragmentToProductDetailFragment(productId = it)
            findNavController().navigate(action)
        })
        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = itemListAdapter
        }
    }

    private fun addObserver() {
        viewModel.state.observe(viewLifecycleOwner) { state ->
            if (state.data != null) {
                itemListAdapter.updateItems(state.data)
            }
            if (state.errorStringId != null) {
                Toast.makeText(requireContext(), state.errorStringId, Toast.LENGTH_LONG).show()
            }
            setProgressVisibility(state.isLoading)
        }
    }

    private fun setProgressVisibility(isLoading: Boolean) {
        binding.progress.isVisible = isLoading
    }
}
