package com.johnlewis.ctdemo.data.remote

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface JLProductsSearchService {

    @GET("/catalog/products/search/keyword")
    suspend fun searchByKeyword(
        @Query("key") apiKey: String = "AIzaSyDD_6O5gUgC4tRW5f9kxC0_76XRC8W7_mI",
        @Query("q") query: String
    ) : Response<ProductSearchResponse>

    companion object {
        fun build(): JLProductsSearchService {
            val retrofit = Retrofit.Builder()
                .baseUrl("https://api.johnlewis.com/search/api/rest/v2/")
                .client(OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
                .build()
            return retrofit.create(JLProductsSearchService::class.java)
        }
    }
}