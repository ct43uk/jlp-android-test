package com.johnlewis.ctdemo.ui.productdetail

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.johnlewis.ctdemo.data.ApiErrorCodeToMessageMapper
import com.johnlewis.ctdemo.data.JLProductRepository
import com.johnlewis.ctdemo.data.ProductDetailDto
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

data class UiState(
    val isLoading: Boolean = false,
    val errorStringId: Int? = null,
    val data: ProductDetailDto? = null
)

class ProductDetailViewModel(private val application: Application) : AndroidViewModel(application) {

    private val _state = MutableLiveData<UiState>()
    val state: LiveData<UiState> = _state

    fun loadDetail(productId: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val result = JLProductRepository(application).detailFromProductId(id = productId)
            if (result.data != null) {
                _state.postValue(UiState(data = result.data))
            } else {
                _state.postValue(UiState(errorStringId = ApiErrorCodeToMessageMapper.parse(result.errorCode!!)))
            }
        }
    }
}