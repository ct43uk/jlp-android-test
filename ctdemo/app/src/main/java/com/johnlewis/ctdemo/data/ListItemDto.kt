package com.johnlewis.ctdemo.data

import com.johnlewis.ctdemo.data.remote.Product

data class ListItemDto(
    val imageSrc: String,
    val title: String,
    val price: String,
    val productId: String
) {
    companion object {
        fun fromApiProduct(product: Product): ListItemDto {
            return ListItemDto(
                imageSrc = DtoValueConverters.buildImageUrl(product.image),
                title = product.title,
                price = String.format("${DtoValueConverters.mapCurrencyCodeToSymbol(product.price.currency)}${product.price.now}"),
                productId = product.productId
            )
        }
    }
}
