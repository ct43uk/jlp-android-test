package com.johnlewis.ctdemo.data

class DtoValueConverters {

    companion object {
        fun buildImageUrl(url: String): String {
            return if (url.startsWith("//")) {
                String.format("https:${url}")
            } else {
                String.format("https://${url}")
            }
        }

        fun mapCurrencyCodeToSymbol(code: String): String? {
            return mapOf(
                "USD" to "$",
                "EUR" to "€",
                "GBP" to "£",
            )[code]
        }
    }
}