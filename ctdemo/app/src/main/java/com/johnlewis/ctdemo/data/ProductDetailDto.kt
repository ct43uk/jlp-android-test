package com.johnlewis.ctdemo.data

import com.johnlewis.ctdemo.data.remote.Detail

class ProductDetailDto(
     val title: String,
     val price: String,
     val productInfoHtml: String,
     val productCode: String,
     val accessibilityText: String,
     val imageUrls: List<String>
)
{
    companion object {
        fun fromAPiProduct(detail: Detail): ProductDetailDto {
            return ProductDetailDto(
                title = detail.title,
                price = String.format("${DtoValueConverters.mapCurrencyCodeToSymbol(detail.price.currency)}${detail.price.now}"),
                productInfoHtml = detail.details.productInformation,
                productCode = detail.productId,
                accessibilityText = detail.media.images.altText,
                imageUrls = detail.media.images.urls.map { DtoValueConverters.buildImageUrl(it) }
            )
        }
    }
}
