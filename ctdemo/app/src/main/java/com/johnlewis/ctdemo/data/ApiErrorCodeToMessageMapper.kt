package com.johnlewis.ctdemo.data

import com.johnlewis.ctdemo.R

class ApiErrorCodeToMessageMapper {

    companion object {
        fun parse(errorCode: Int): Int {
            return when (errorCode) {
                403 -> R.string.api_error_403
                404 -> R.string.api_error_404
                500 -> R.string.api_error_500
                else -> R.string.api_error_generic
            }
        }
    }
}